<?php

namespace Itbinc\ThemeInstaller;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Package\PackageInterface;
use Composer\Installer\LibraryInstaller;

class ThemeInstaller extends LibraryInstaller
{
    const DEFAULT_ROOT = "Themes";

    /**
     * Get the fully-qualified install path
     * {@inheritDoc}
     */
    public function getInstallPath(PackageInterface $package)
    {
        return $this->getBaseInstallationPath() . '/' . $this->getModuleName($package);
    }

    /**
     * Get the base path that the theme should be installed into.
     * Defaults to Theme/ and can be overridden in the theme's composer.json.
     * @return string
     */
    protected function getBaseInstallationPath()
    {
        if (!$this->composer || !$this->composer->getPackage()) {
            return self::DEFAULT_ROOT;
        }

        $extra = $this->composer->getPackage()->getExtra();

        if (!$extra || empty($extra['theme-dir'])) {
            return self::DEFAULT_ROOT;
        }

        return $extra['theme-dir'];
    }

    /**
     * Get the theme name, i.e. "itbinc/default-theme" will be transformed into "Something"
     * @param PackageInterface $package
     * @return string
     * @throws \Exception
     */
    protected function getModuleName(PackageInterface $package)
    {
        $name = $package->getPrettyName();
        $split = explode("/", $name);

        if (count($split) !== 2) {
            throw new \Exception($this->usage());
        }

        $splitNameToUse = explode("-", $split[1]);

        if (count($splitNameToUse) < 2) {
            throw new \Exception($this->usage());
        }

        if (array_pop($splitNameToUse) !== 'theme') {
            throw new \Exception($this->usage());
        }

        return implode('',array_map('ucfirst', $splitNameToUse));
    }

    /**
     * Get the usage instructions
     * @return string
     */
    protected function usage()
    {
        return "Ensure your package's name is in the format <vendor>/<name>-<theme>";
    }

    /**
     * {@inheritDoc}
     */
    public function supports($packageType)
    {
        return 'itb-theme' === $packageType;
    }
}
